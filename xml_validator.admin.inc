<?php
// $Id:$

/**
 * @file
 *   This include file implements the forms for xml validator.
 */

/**
 * XML validator form.
 * 
 * @param array $form
 *   XML validator form.
 *
 * @param array $form_state
 *   XML validator form state.
 */
function xml_validator_form($form, $form_state) {
  $form['header'] = array(
    '#markup' => t(
'<h2>XML Validator</h2><br />
<h5>Upload your XML and XSD files here. In case if the files are large and not getting uploaded,
you can directly upload the files on server using ftp client and use the drush command \'drush xml-validate\'
instead of this form to validate your XML file.</h5><br />
    '),
    '#prefix' => '<div class="xml-validator-header">',
    '#suffix' => '</div>',
  );

  $form['xml'] = array(
    '#type' => 'file',
    '#title' => t('Upload XML file'),
    '#title_display' => 'before',
    '#size' => 22,
    '#prefix' => '<div class="xml-validator-xml-upload">',
    '#description' => t('Upload XML file here.'),
    '#suffix' => '</div>',
  );

  $form['xsd'] = array(
    '#type' => 'file',
    '#title' => t('Upload XSD file'),
    '#title_display' => 'before',
    '#size' => 22,
    '#prefix' => '<div class="xml-validator-xsd-upload">',
    '#description' => t('Upload XSD file here.'),
    '#suffix' => '</div>',
  );

  // Submit button and handler.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Validate XML!'),
    '#prefix' => '<div class="xml-validator-submit">',
    '#suffix' => '</div>',
    '#submit' => array('xml_validator_form_submit'),
  );

  // Validation function.
  $form['#validate'][] = 'xml_validator_form_validate';

  return $form;
}

/**
 * Validation handler for xml validator form.
 * 
 * @param array $form
 *   XML validator form.
 *
 * @param array $form_state
 *   XML validator form state.
 */
function xml_validator_form_validate($form, &$form_state) {
  // To validate xml input file.
  $xml_validators = array(
    'file_validate_extensions' => array('xml'),
  );
  // To validate xsd input file.
  $xsd_validators = array(
    'file_validate_extensions' => array('xsd'),
  );

  // Save the xml and xsd files at temporary location.
  $xml_file = file_save_upload('xml', $xml_validators, FALSE, FILE_EXISTS_RENAME);
  $xsd_file = file_save_upload('xsd', $xsd_validators, FALSE, FILE_EXISTS_RENAME);

  // If xml or xsd upload fails then display error message.
  if ($xml_file === FALSE || $xml_file === NULL) {
    form_set_error('xml', t('Please upload XML files only.'));
  }
  elseif ($xsd_file === FALSE || $xsd_file === NULL) {
    form_set_error('xsd', t('Please upload XSD files only.'));
  }
  else {
    // File uploaded, assigning values to form_state.
    $form_state['values']['xml'] = $xml_file;
    $form_state['values']['xsd'] = $xsd_file;
  }
}

/**
 * Submit handler for xml validator form.
 * 
 * @param array $form
 *   XML validator form.
 *
 * @param array $form_state
 *   XML validator form state.
 */
function xml_validator_form_submit($form, &$form_state) {
  $xml = $form_state['values']['xml']->destination;
  $xsd = $form_state['values']['xsd']->destination;
  $return = xml_validator_xml_validate_callback($xml, $xsd);
  $file_temporay_path = variable_get('file_temporary_path');

  switch ($return) {
    case 'xml_not_found':
      drupal_set_message(t('XML file not found at URL: @xml, please try again.', array('@xml' => $xml)), 'error');
      break;

    case 'log_write_success':
      drupal_set_message(t(
'XML file was processed successfully, please click @log to see the log',
        array(
          '@log' => l('Error Log', $file_temporay_path . 'xml_error_log.txt', array()),
        )
      ), 'status');
      break;
    case 'log_not_writable':
      drupal_set_message(t(
'Error log file does not have writable permission. Please give writable permission to your @folder',
        array('@folder' => $file_temporay_path)
      ), 'error');
      break;
    case 'error_free_xml':
      drupal_set_message(t('XML file does not have any error! You are good to go.'), 'status');
    default:
      break;
  }
}

