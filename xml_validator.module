<?php
/**
 * @file
 * Render an administrative menu as a dropdown menu at the top of the window.
 *  The XML Validator module is used to validate xml.
 *  This module takes in xml and xsd location as input.
 *
 *  It also provides optional XML input (on-submit) checks for 
 *  well-formedness and schema-validation.
 *
 *  @author Raviish Gupta (ravyg)
 */

/**
 * Implements hook_help().
 */
function xml_validator_help($path) {
  switch ($path) {
    case 'admin/help#xml_validator':
      return t('Welcome to XML Validator. <p>Get XML validated here!</p>');
  }
}

/**
 * Implements hook_permission().
 */
function xml_validator_permission() {
  return array(
    'use xml validator' => array(
      'title' => t('Use XML Validator'),
    ),
    'configure xml validator' => array(
      'title' => t('Configure XML Validator'),
    ),
  );
}

/**
 * Implements hook_ctools_plugin_directory().
 */
function xml_validator_ctools_plugin_directory($module, $plugin) {
  if ($module == 'ctools' && !empty($plugin)) {
    return 'plugins/' . $plugin;
  }
}

/**
 * Implements hook_menu().
 *
 * Pages associated with XML validator.
 */
function xml_validator_menu() {
  $items['admin/xml-validator'] = array(
    'title' => t('XML Validator'),
    'description' => t('Get your XML files validated here against your XML schemas.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('xml_validator_form'),
    'access arguments' => array('use xml validator'),
    'file' => 'xml_validator.admin.inc',
    'file path' => drupal_get_path('module', 'xml_validator'),
  );

  return $items;
}

/**
 * Function for fetching xml content.
 *
 * @param string $xml
 *   Xml file.
 * @param string $xsd
 *   Xsd file.
 */
function xml_validator_xml_validate_callback($xml, $xsd) {
  // Loading XML file.
  libxml_use_internal_errors(true);
  if (file_exists($xml)) {
    $xml_data_string = file_get_contents($xml);
    $return = _xml_validator_xml_validate_process($xml_data_string, $xsd);
  }
  else {
    $return = 'xml_not_found';
  }

  return $return;
}

/**
 * Function for processing XML against XSD.
 *
 * @param string $xml_data_string
 *   A structured array xml data string.
 * @param string $xsd
 *   Xsd file.
 */
function _xml_validator_xml_validate_process($xml_data_string, $xsd) {
  $xml = new DOMDocument();
  $xml->loadXML($xml_data_string, LIBXML_NOBLANKS); // Or load if filename required
  if (!$xml->schemaValidate($xsd)) {
    $err = libxml_get_errors();
    $error_message = array();
    $error_line_no = array();
    foreach ($err as $err_details) {
      $error_message[] = $err_details->message;
      $error_line_no[] = $err_details->line;
    }
    $unique_errors = array_unique($error_message);
    $unique_errors[] = 'Errors in expected line numbers : ' . implode(', ', array_unique($error_line_no));
    $output = NULL;
    foreach ($unique_errors as $key => $value) {
      $output .= 'Line ' . $key . ' - ' . $value;
    }

    $error_log = 'temporary://xml_error_log.txt';
    if (file_put_contents($error_log, $output) > 0) {
      $return = 'log_write_success';
    }
    else {
      $return = 'log_not_writable';
    }
  }
  else {
    $return = 'error_free_xml';
  }

  return $return;
}
