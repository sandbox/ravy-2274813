<?php
// $Id:$

/**
 * The XML Validator drush file contains drush command to validate large XML
 * and XSD files. It provides an interactive commandline drush function to enter
 * paths of XML, XSD and Error log files placed on server.
 * 
 * @author Raviish Gupta (ravyg)
 */


/**
 * Implements hook_drush_command().
 * 
 * XML file validator against the schema file.
 */
function xml_validator_drush_command() {
  $items['xml-validate'] = array(
    'description' => 'Validate the xml file against the xsd schema file.',
  );

  return $items;
}

/**
 * Callback of drush xml-validate.
 */
function drush_xml_validator_xml_validate() {
  if (drush_confirm(
'Please keep a note of the paths of the xml, xsd and the error_log.txt file on your server before continuing, press y to proceed, n to abort:'
  )) {
    $xml = drush_prompt(dt('Absolute path of the xml file(e.g: /var/www/input.xml) '));
    $xsd = drush_prompt(dt('Absolute path of the xsd file(e.g: /var/www/schema.xsd) '));

    module_load_include('inc', 'xml_validator', 'xml_validator.admin');
    $return = xml_validator_xml_validate_callback($xml, $xsd);
    $file_temporay_path = variable_get('file_temporary_path');

    switch ($return) {
      case 'xml_not_found':
        drush_log('XML file not found at URL: ' . $xml, 'error');
        break;
      case 'log_write_success':
        drush_log(
'XML file was processed successfully and errors were found, please check the log: ' . $file_temporay_path . '/xml_error_log.txt',
        'ok');
        break;
      case 'log_not_writable':
        drush_log(
'Error log file does not have writable permission. Please give writable permission to your ' . $file_temporay_path . ' folder.',
        'error');
        break;
      case 'error_free_xml':
        drush_log('XML file does not have any error! You are good to go.', 'success');
      default:
        break;
    }
  }
  else {
    drush_user_abort();
  }
}
